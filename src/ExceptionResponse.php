<?php

namespace Lliure\Core\Response;

use Laminas\Diactoros\Exception\ExceptionInterface;
use Laminas\Diactoros\Response;
use League\Route\Http\Exception\HttpExceptionInterface;
use Lliure\Core\Exception\DataException;

class ExceptionResponse extends \Laminas\Diactoros\Response\JsonResponse
{

	protected string $reasonPhrase;
	protected string $message;
	protected int $codeStatus;
	protected string $file;
	protected int $line;
	protected array $trace;
	protected ?\Throwable $previous;
	
	public function __construct(?\Throwable $exeption = null){

		if ($exeption instanceof DataException){
			parent::__construct($exeption->getData(), $exeption->getCode() > 599 ? 500 : $exeption->getCode());
			return;
		}
		
		if($exeption instanceof HttpExceptionInterface){
			$response = $exeption->buildJsonResponse(new Response);
			parent::__construct([
				'status_code'   => $response->getStatusCode(),
				'reason_phrase' => $response->getReasonPhrase(),
			], $exeption->getStatusCode(), $exeption->getHeaders());
			return;
		}

		if($exeption instanceof ExceptionInterface){
			parent::__construct([
				'status_code'   => $exeption->getCode(),
				'reason_phrase' => $exeption->getMessage(),
			], $exeption->getCode());
			return;
		}

		$codeStatus = $code = $exeption->getCode();
		$code = (($code < 100 || $code >= 600)? 500: $code);
		$reasonPhrase = $exeption->getMessage();
		$message = strtok($reasonPhrase, "\n");

		$this->setReasonPhrase($reasonPhrase);
		$this->setMessage($message);
		$this->setCodeStatus($codeStatus);
		$this->setFile($exeption->getFile());
		$this->setLine($exeption->getLine());
		$this->setTrace(array_map(fn($i) => array_diff_key($i, ['args' => 0]), $exeption->getTrace()));
		$this->previous = $exeption;

		$errorData = [
			'status_code'   => &$this->codeStatus,
			'reason_phrase' => &$this->reasonPhrase,
			'code'          => &$code,
			'message'       => &$this->message,
			'file'          => &$this->file,
			'line'          => &$this->line,
			'trace'         => &$this->trace,
		];

		parent::__construct($errorData, $code);
	}
	
	/**
	 * @param string $reasonPhrase
	 */
	public function setReasonPhrase(string $reasonPhrase): void{
		$this->reasonPhrase = $reasonPhrase;
	}
	
	/**
	 * @param string $message
	 */
	public function setMessage(string $message): void{
		$this->message = $message;
	}
	
	/**
	 * @param int $codeStatus
	 */
	public function setCodeStatus(int $codeStatus): void{
		$this->codeStatus = $codeStatus;
	}
	
	/**
	 * @param string $file
	 */
	public function setFile(string $file): void{
		$this->file = $file;
	}
	
	/**
	 * @param int $line
	 */
	public function setLine(int $line): void{
		$this->line = $line;
	}
	
	/**
	 * @param array $trace
	 */
	public function setTrace(array $trace): void{
		$this->trace = $trace;
	}
}